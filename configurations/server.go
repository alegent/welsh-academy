package configurations

import (
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/controllers"
	"gitlab.com/alegent/welsh-academy/middlewares"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/views"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// WelshServer
// Function to return an already configured server (e.g.: routes are defined)
// It seems that group could be used more, however it really makes the code harder to read.
// I preferred redundancy to keep it clean and configurable on an endpoint basis.
func WelshServer(db *gorm.DB) *gin.Engine {
	server := gin.Default()

	server.GET("/health", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"status": "UP"})
	})

	group := server.Group("", func(ctx *gin.Context) {
		ctx.Set(constants.CtxDb, db)
	})

	{
		group.POST(
			"/users",
			middlewares.Authentication(db),
			middlewares.Role(false, constants.Admin),
			middlewares.RequestBind[views.NewUserRequest],
			controllers.NewUser,
		)

		group.GET(
			"/users/:id",
			middlewares.Authentication(db),
			middlewares.UriBind[models.User],
			middlewares.Role(true, constants.Admin),
			controllers.GetUser,
		)

		group.PUT(
			"/users/:id",
			middlewares.Authentication(db),
			middlewares.UriBind[models.User],
			middlewares.Role(true, constants.Admin),
			middlewares.RequestBind[views.UpdateUserRequest],
			controllers.UpdateUser,
		)

		group.DELETE(
			"/users/:id",
			middlewares.Authentication(db),
			middlewares.UriBind[models.User],
			middlewares.Role(true, constants.Admin),
			controllers.DeleteUser,
		)

		group.GET(
			"/users/:id/recipes",
			middlewares.UriBind[models.User],
			controllers.GetUserRecipes,
		)

		group.GET(
			"/users/:id/starred",
			middlewares.UriBind[models.User],
			controllers.GetUserStarredRecipes,
		)
	}

	{
		group.GET(
			"/ingredients",
			middlewares.PageBind,
			controllers.GetIngredients,
		)

		group.GET(
			"/ingredients/:id",
			middlewares.UriBind[models.Ingredient],
			controllers.GetIngredient,
		)

		group.POST(
			"/ingredients",
			middlewares.Authentication(db),
			middlewares.Role(false, constants.CheddarLover, constants.Admin),
			middlewares.RequestBind[views.NewIngredientRequest],
			controllers.NewIngredient,
		)

		group.DELETE(
			"/ingredients/:id",
			middlewares.Authentication(db),
			middlewares.Role(false, constants.CheddarLover, constants.Admin),
			middlewares.UriBind[models.Ingredient],
			controllers.DeleteIngredient,
		)
	}

	{
		group.GET(
			"/recipes",
			middlewares.PageBind,
			controllers.GetRecipes,
		)

		group.GET(
			"/recipes/:id",
			middlewares.UriBind[models.Recipe],
			controllers.GetRecipe,
		)

		group.POST(
			"/recipes",
			middlewares.Authentication(db),
			middlewares.Role(false, constants.CheddarLover, constants.Admin),
			middlewares.RequestBind[views.NewRecipeRequest],
			controllers.NewRecipe,
		)

		group.PUT(
			"/recipes/:id",
			middlewares.Authentication(db),
			middlewares.Role(false, constants.CheddarLover, constants.Admin),
			middlewares.UriBind[models.Recipe],
			middlewares.RequestBind[views.UpdateRecipeRequest],
			controllers.UpdateRecipe,
		)
		group.DELETE(
			"/recipes/:id",
			middlewares.Authentication(db),
			middlewares.Role(false, constants.CheddarLover, constants.Admin),
			middlewares.UriBind[models.Recipe],
			controllers.DeleteRecipe,
		)

		group.PUT(
			"/recipes/:id/star",
			middlewares.Authentication(db),
			middlewares.UriBind[models.Recipe],
			controllers.StarRecipe,
		)
		group.DELETE(
			"/recipes/:id/star",
			middlewares.Authentication(db),
			middlewares.UriBind[models.Recipe],
			controllers.UnstarRecipe,
		)
	}

	return server
}

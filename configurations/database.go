package configurations

import (
	"gitlab.com/alegent/welsh-academy/models"
	"log"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func WelshDatabase() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	if err != nil {
		log.Fatalln("db connection failed", err)
	}

	if err := db.AutoMigrate(&models.User{}, &models.Ingredient{}, &models.Recipe{}); err != nil {
		log.Fatalln("auto migration failed", err)
	}

	return db
}

func CloseDatabase(db *gorm.DB) {
	sqldb, err := db.DB()

	if err != nil {
		log.Fatalln("could not retrieve sqlite db", err)
	}

	if err := sqldb.Close(); err != nil {
		log.Fatalln("could not close sqlite db", err)
	}
}

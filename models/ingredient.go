package models

import (
	"time"

	"gorm.io/gorm"
)

type Ingredient struct {
	ID           string `gorm:"primaryKey" uri:"id" binding:"required,uuid"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    gorm.DeletedAt `gorm:"index"`
	Name         string
	IsGlutenFree bool
	CreatedBy    string
}

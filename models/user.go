package models

import (
	"gitlab.com/alegent/welsh-academy/constants"
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID          string `gorm:"primaryKey" uri:"id" binding:"required,uuid"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt `gorm:"index"`
	Username    string         `gorm:"uniqueIndex"`
	Password    string
	Role        constants.Role `gorm:"type=integer"`
	StarRecipes []Recipe       `gorm:"many2many:user_has_star_recipe;"`
}

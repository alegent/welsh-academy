package models

import (
	"golang.org/x/exp/slices"
	"time"

	"gorm.io/gorm"
)

type Recipe struct {
	ID          string `gorm:"primaryKey" uri:"id" binding:"required,uuid"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt `gorm:"index"`
	Name        string
	Description *string
	Ingredients []Ingredient `gorm:"many2many:recipe_ingredients;"`
	CreatedBy   string
}

func (self *Recipe) IsGlutenFree() bool {
	index := slices.IndexFunc(self.Ingredients, func(ingredient Ingredient) bool {
		return !ingredient.IsGlutenFree
	})

	return index == -1
}

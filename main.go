package main

import (
	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/configurations"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"gorm.io/gorm"
	"log"
	"os"
	"time"
)

func main() {
	db := configurations.WelshDatabase()
	defer configurations.CloseDatabase(db)
	server := configurations.WelshServer(db)

	seed := func(db *gorm.DB) {
		username, usernameExist := os.LookupEnv("WELSH_USERNAME")
		password, passwordExist := os.LookupEnv("WELSH_PASSWORD")

		if !usernameExist || !passwordExist {
			return
		}

		user := models.User{
			// the uuid is hard coded just to be able to quickly test through postman
			ID:          uuid.NewString(),
			CreatedAt:   time.Now(),
			UpdatedAt:   time.Now(),
			DeletedAt:   gorm.DeletedAt{},
			Username:    username,
			Password:    utils.MultiphaseHash(constants.HashSalt+password, constants.HashPass),
			Role:        constants.Admin,
			StarRecipes: []models.Recipe{},
		}

		if err := db.Save(&user).Error; err != nil {
			log.Fatalf("could not save user %s%v\n", username, err)
		}
	}

	seed(db)

	if err := server.Run(); err != nil {
		log.Fatalln("server could not start", err)
	}
}

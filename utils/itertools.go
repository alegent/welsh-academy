package utils

import "golang.org/x/exp/slices"

func First[T any](slice []T, f func(T) bool) *T {
	if i := slices.IndexFunc(slice, f); i != -1 {
		return &slice[i]
	}

	return nil
}

func Map[T any, R any](slice []T, f func(T) R) (res []R) {
	for _, element := range slice {
		res = append(res, f(element))
	}

	return res
}

func Remove[T any](slice []T, i int) []T {
	return append(slice[:i], slice[i+1:]...)
}

func RemoveFunc[T any](slice []T, f func(T) bool) (res []T) {
	res = slice

	for {
		i := slices.IndexFunc(res, f)

		if i == -1 {
			break
		}

		res = Remove(res, i)
	}

	return res
}

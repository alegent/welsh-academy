package utils

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alegent/welsh-academy/views"
	"log"
	"net/http"
)

// ContextGet
// Use generic to have a typed `ctx.Get` and avoid the cast dance everytime
func ContextGet[T any, P *T](ctx *gin.Context, key string) P {
	element, exist := ctx.Get(key)

	if !exist {
		return nil
	}

	ref := element.(T)
	return &ref
}

func ContextAbortWithMistake(ctx *gin.Context, status int, details ...views.MistakeDetail) {
	ctx.AbortWithStatusJSON(status, views.New(status, details...))
}

func ContextAbortWithError(ctx *gin.Context, err error) {
	log.Println(err)
	ContextAbortWithMistake(ctx, http.StatusInternalServerError)
}

package utils

import (
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func TestFirst(t *testing.T) {
	slice := []int{1, 2, 3}
	expected := 2
	assert.Equal(t, expected, *First(slice, func(i int) bool { return i == 2 }))
}

func TestMap(t *testing.T) {
	slice := []int{1, 2, 3}
	expected := []string{"1", "2", "3"}
	assert.Equal(t, expected, Map(slice, strconv.Itoa))
}

func TestRemove(t *testing.T) {
	tcs := []struct {
		name     string
		slice    []int
		index    int
		expected []int
	}{
		{
			name:     "remove first index",
			slice:    []int{1, 2, 3},
			index:    0,
			expected: []int{2, 3},
		},
		{
			name:     "remove last index",
			slice:    []int{1, 2, 3},
			index:    2,
			expected: []int{1, 2},
		},
		{
			name:     "remove somewhere in the middle",
			slice:    []int{1, 2, 3},
			index:    1,
			expected: []int{1, 3},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			assert.Equal(t, tc.expected, Remove(tc.slice, tc.index))
		})
	}
}

func TestRemoveFunc(t *testing.T) {
	tcs := []struct {
		name      string
		slice     []int
		predicate func(int) bool
		expected  []int
	}{
		{
			name:  "remove one element",
			slice: []int{1, 2, 3},
			predicate: func(i int) bool {
				return i == 1
			},
			expected: []int{2, 3},
		},
		{
			name:  "remove all element",
			slice: []int{1, 1, 1, 1},
			predicate: func(i int) bool {
				return i == 1
			},
			expected: []int{},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			assert.Equal(t, tc.expected, RemoveFunc(tc.slice, tc.predicate))
		})
	}
}

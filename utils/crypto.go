package utils

import (
	"crypto/sha512"
	"fmt"
)

// MultiphaseHash Make a sha512 hash of a string, with a certain amount of pass
func MultiphaseHash(str string, passes int) string {
	bytes := []byte(str)

	for i := 0; i < passes; i++ {
		hash := sha512.New()
		hash.Write(bytes)
		bytes = hash.Sum(nil)
	}

	return fmt.Sprintf("%x", bytes)
}

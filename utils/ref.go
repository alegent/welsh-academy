package utils

func Ref[T any](element T) *T {
	return &element
}

func UnwrapOr[T any, P *T](ref P, or T) T {
	if ref != nil {
		return *ref
	}

	return or
}

FROM golang:alpine AS build

WORKDIR /root
COPY . .

RUN apk add --no-cache musl-dev gcc sqlite && go build .

FROM alpine

COPY --from=build /root/welsh-academy .

CMD ["./welsh-academy"]
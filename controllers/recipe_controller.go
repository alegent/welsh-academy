package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"gitlab.com/alegent/welsh-academy/views"
	"gorm.io/gorm"
	"net/http"
	"time"
)

func GetRecipes(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	page := utils.ContextGet[views.Page](ctx, constants.CtxPageBind)

	var recipes []models.Recipe
	query := db.Limit(page.Length)
	query = query.Offset(page.Offset)
	query = query.Order("created_at asc")
	query = query.Where("name like ?", fmt.Sprintf("%%%s%%", ctx.Query("s")))
	query = query.Preload("Ingredients")

	if err := query.Find(&recipes).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, utils.Map(recipes, RecipeIntoResponse))
}

func GetRecipe(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	recipe := utils.ContextGet[models.Recipe](ctx, constants.CtxUriBind)

	if err := db.Preload("Ingredients").Where(&recipe).First(&recipe).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, RecipeIntoResponse(*recipe))
}

func NewRecipe(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	user := utils.ContextGet[models.User](ctx, constants.CtxUser)
	request := utils.ContextGet[views.NewRecipeRequest](ctx, constants.CtxRequestBind)

	var details []views.MistakeDetail
	invalidIngredientDetail := views.MistakeDetail{
		Error:       "INVALID_INGREDIENTS",
		Description: "'ingredients' cannot be empty or invalid",
		Field:       utils.Ref("ingredients"),
	}

	if request.Name == "" {
		details = append(details, views.MistakeDetail{
			Error:       "INVALID_NAME",
			Description: "'name' cannot be empty or invalid",
			Field:       utils.Ref("name"),
		})
	}

	if len(request.Ingredients) <= 0 {
		details = append(details, invalidIngredientDetail)
	}

	var ingredients []models.Ingredient
	if err := db.Where("id in ?", request.Ingredients).Find(&ingredients).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	if len(ingredients) != len(request.Ingredients) {
		details = append(details, invalidIngredientDetail)
	}

	if len(details) > 0 {
		utils.ContextAbortWithMistake(ctx, http.StatusBadRequest, details...)
		return
	}

	recipe := models.Recipe{
		ID:          uuid.NewString(),
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
		DeletedAt:   gorm.DeletedAt{},
		Name:        request.Name,
		Description: request.Description,
		Ingredients: ingredients,
		CreatedBy:   user.ID,
	}

	if err := db.Save(&recipe).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusCreated, RecipeIntoResponse(recipe))
}

func UpdateRecipe(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	request := utils.ContextGet[views.UpdateRecipeRequest](ctx, constants.CtxRequestBind)
	recipe := utils.ContextGet[models.Recipe](ctx, constants.CtxUriBind)

	if err := db.Preload("Ingredients").Where(&recipe).First(&recipe).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	if request.Description != nil && *request.Description != "" {
		recipe.Description = request.Description
	}

	if request.Ingredients != nil && len(request.Ingredients) > 0 {
		var ingredients []models.Ingredient

		if err := db.Where("id in ?", request.Ingredients).Find(&ingredients).Error; err != nil {
			utils.ContextAbortWithError(ctx, err)
			return
		}

		if len(request.Ingredients) != len(ingredients) {
			utils.ContextAbortWithMistake(ctx, http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_INGREDIENTS",
				Description: "'ingredients' cannot be empty or invalid",
				Field:       utils.Ref("ingredients"),
			})

			return
		}

		recipe.Ingredients = ingredients
	}

	if err := db.Save(recipe).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, RecipeIntoResponse(*recipe))
}

func DeleteRecipe(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	recipe := utils.ContextGet[models.Recipe](ctx, constants.CtxUriBind)

	if err := db.Delete(&recipe).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.Status(http.StatusNoContent)
}

func StarRecipe(ctx *gin.Context) {
	user := utils.ContextGet[models.User](ctx, constants.CtxUser)
	recipe := utils.ContextGet[models.Recipe](ctx, constants.CtxUriBind)
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	db = db.Preload("StarRecipes").Where(&user).First(&user)

	if err := db.Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	user.StarRecipes = append(user.StarRecipes, *recipe)

	if err := db.Save(&user).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.Status(http.StatusNoContent)
}

func UnstarRecipe(ctx *gin.Context) {
	user := utils.ContextGet[models.User](ctx, constants.CtxUser)
	recipe := utils.ContextGet[models.Recipe](ctx, constants.CtxUriBind)
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	db = db.Preload("StarRecipes").Where(&user).First(&user)

	if err := db.Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	user.StarRecipes = utils.RemoveFunc(user.StarRecipes, func(element models.Recipe) bool {
		return element.ID == recipe.ID
	})

	if err := db.Save(&user).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.Status(http.StatusNoContent)
}

func RecipeIntoResponse(recipe models.Recipe) views.RecipeResponse {
	return views.RecipeResponse{
		ID:          recipe.ID,
		CreatedAt:   recipe.CreatedAt,
		UpdatedAt:   recipe.UpdatedAt,
		Name:        recipe.Name,
		Description: recipe.Description,
		Ingredients: utils.Map(recipe.Ingredients, IngredientIntoResponse),
		CreatedBy:   recipe.CreatedBy,
	}
}

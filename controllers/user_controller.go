package controllers

import (
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/views"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/utils"
	"gorm.io/gorm"
)

func GetUser(ctx *gin.Context) {
	user := utils.ContextGet[models.User](ctx, constants.CtxUriBind)
	ctx.JSON(http.StatusOK, UserIntoResponse(user))
}

func NewUser(ctx *gin.Context) {
	var details []views.MistakeDetail
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	request := utils.ContextGet[views.NewUserRequest](ctx, constants.CtxRequestBind)

	if request.Username == "" {
		details = append(details, views.MistakeDetail{
			Error:       "INVALID_USERNAME",
			Description: "username should not be empty or invalid",
			Field:       utils.Ref("username"),
		})
	}

	if request.Password == "" {
		details = append(details, views.MistakeDetail{
			Error:       "INVALID_PASSWORD",
			Description: "password should not be empty or invalid",
			Field:       utils.Ref("password"),
		})
	}

	if len(details) != 0 {
		utils.ContextAbortWithMistake(ctx, http.StatusBadRequest, details...)
		return
	}

	user := models.User{
		ID:        uuid.NewString(),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		DeletedAt: gorm.DeletedAt{},
		Username:  request.Username,
		Password:  utils.MultiphaseHash(constants.HashSalt+request.Password, constants.HashPass),
		Role:      utils.UnwrapOr(request.Role, constants.Basic),
	}

	if err := db.Create(&user).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusCreated, UserIntoResponse(&user))
}

func UpdateUser(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	authenticated := utils.ContextGet[models.User](ctx, constants.CtxUser)
	user := utils.ContextGet[models.User](ctx, constants.CtxUriBind)
	request := utils.ContextGet[views.UpdateUserRequest](ctx, constants.CtxRequestBind)

	if request.Password != nil {
		user.Password = utils.MultiphaseHash(constants.HashSalt+*request.Password, constants.HashPass)
	}

	if request.Role != nil {
		if authenticated.Role != constants.Admin {
			utils.ContextAbortWithMistake(ctx, http.StatusForbidden)
			return
		}

		user.Role = *request.Role
	}

	if err := db.Save(&user).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, UserIntoResponse(user))
}

func DeleteUser(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	user := utils.ContextGet[models.User](ctx, constants.CtxUriBind)

	if err := db.Delete(&user).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.Status(http.StatusNoContent)
}

func GetUserRecipes(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	user := utils.ContextGet[models.User](ctx, constants.CtxUriBind)

	var recipes []models.Recipe

	if err := db.Where(models.Recipe{CreatedBy: user.ID}).Find(&recipes).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, utils.Map(recipes, RecipeIntoResponse))
}

func GetUserStarredRecipes(ctx *gin.Context) {
	user := utils.ContextGet[models.User](ctx, constants.CtxUriBind)
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)

	if err := db.Preload("StarRecipes").Where(&user).First(&user).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, utils.Map(user.StarRecipes, RecipeIntoResponse))
}

func UserIntoResponse(user *models.User) views.UserResponse {
	return views.UserResponse{
		ID:        user.ID,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
		Username:  user.Username,
		Role:      user.Role,
	}
}

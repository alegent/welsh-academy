package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"gitlab.com/alegent/welsh-academy/views"
	"gorm.io/gorm"
	"net/http"
	"time"
)

func GetIngredients(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	page := utils.ContextGet[views.Page](ctx, constants.CtxPageBind)

	var ingredients []models.Ingredient
	query := db.Limit(page.Length)
	query = query.Offset(page.Offset)
	query = query.Order("created_at asc")
	query = query.Where("name like ?", fmt.Sprintf("%%%s%%", ctx.Query("s")))

	if err := query.Find(&ingredients).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, utils.Map(ingredients, IngredientIntoResponse))
}

func GetIngredient(ctx *gin.Context) {
	ingredient := utils.ContextGet[models.Ingredient](ctx, constants.CtxUriBind)
	ctx.JSON(http.StatusOK, IngredientIntoResponse(*ingredient))
}

func NewIngredient(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	user := utils.ContextGet[models.User](ctx, constants.CtxUser)
	request := utils.ContextGet[views.NewIngredientRequest](ctx, constants.CtxRequestBind)

	if request.Name == "" {
		utils.ContextAbortWithMistake(ctx, http.StatusBadRequest, views.MistakeDetail{
			Error:       "INVALID_NAME",
			Description: "'name' cannot be empty or invalid",
			Field:       utils.Ref("name"),
		})

		return
	}

	ingredient := models.Ingredient{
		ID:           uuid.NewString(),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    gorm.DeletedAt{},
		Name:         request.Name,
		IsGlutenFree: utils.UnwrapOr(request.IsGlutenFree, false),
		CreatedBy:    user.ID,
	}

	if err := db.Create(&ingredient).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.JSON(http.StatusCreated, IngredientIntoResponse(ingredient))
}

func DeleteIngredient(ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)
	ingredient := utils.ContextGet[models.Ingredient](ctx, constants.CtxUriBind)

	if err := db.Delete(&ingredient).Error; err != nil {
		utils.ContextAbortWithError(ctx, err)
		return
	}

	ctx.Status(http.StatusNoContent)
}

func IngredientIntoResponse(ingredient models.Ingredient) views.IngredientResponse {
	return views.IngredientResponse{
		ID:           ingredient.ID,
		CreatedAt:    ingredient.CreatedAt,
		UpdatedAt:    ingredient.UpdatedAt,
		Name:         ingredient.Name,
		IsGlutenFree: ingredient.IsGlutenFree,
		CreatedBy:    ingredient.CreatedBy,
	}
}

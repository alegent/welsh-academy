package middlewares

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/utils"
	"gorm.io/gorm"
	"net/http"
)

func UriBind[T any](ctx *gin.Context) {
	db := *utils.ContextGet[*gorm.DB](ctx, constants.CtxDb)

	var bind T

	if err := ctx.ShouldBindUri(&bind); err != nil {
		utils.ContextAbortWithMistake(ctx, http.StatusNotFound)
		return
	}

	if err := db.Where(bind).First(&bind).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			utils.ContextAbortWithMistake(ctx, http.StatusNotFound)
			return
		}

		utils.ContextAbortWithError(ctx, db.Error)
		return
	}

	ctx.Set(constants.CtxUriBind, bind)
}

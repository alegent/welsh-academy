package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"golang.org/x/exp/slices"
	"net/http"
)

func Role(itself bool, allowed ...constants.Role) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authenticated := utils.ContextGet[models.User](ctx, constants.CtxUser)
		user := utils.ContextGet[models.User](ctx, constants.CtxUriBind)

		if itself && user.ID == authenticated.ID {
			return
		}

		if !slices.Contains(allowed, authenticated.Role) {
			utils.ContextAbortWithMistake(ctx, http.StatusForbidden)
			return
		}
	}
}

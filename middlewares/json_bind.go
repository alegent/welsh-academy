package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/utils"
	"net/http"
)

func RequestBind[T any](ctx *gin.Context) {
	var request T

	if err := ctx.ShouldBindJSON(&request); err != nil {
		utils.ContextAbortWithMistake(ctx, http.StatusBadRequest)
		return
	}

	ctx.Set(constants.CtxRequestBind, request)
}

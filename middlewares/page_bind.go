package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/views"
	"strconv"
)

func PageBind(ctx *gin.Context) {
	length, _ := strconv.Atoi(ctx.Query("length"))
	offset, _ := strconv.Atoi(ctx.Query("offset"))

	if length <= 0 {
		length = 10
	}

	if offset < 1 {
		offset = 1
	}

	ctx.Set(constants.CtxPageBind, views.Page{
		Length: length,
		Offset: offset - 1,
	})
}

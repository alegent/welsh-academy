package middlewares

import (
	"encoding/base64"
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"gorm.io/gorm"
	"net/http"
	"regexp"
	"strings"
)

// Authentication
// This is an authentication middleware that (really) quickly parse the authorization header,
// and try to find a user that have a corresponding username and hash password
//
// Note: I was wishing to use the `gin.BasicAuth` middleware instead, but it's seem too much basic (punch unintended)
// to handle properly a list of always growing, database saved, users.
func Authentication(db *gorm.DB) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authorization := ctx.GetHeader("authorization")
		expression := regexp.MustCompile("[Bb]asic (.*)$")
		matches := expression.FindStringSubmatch(authorization)

		if len(matches) != 2 {
			utils.ContextAbortWithMistake(ctx, http.StatusUnauthorized)
			return
		}

		decoded, err := base64.StdEncoding.DecodeString(matches[1])

		if err != nil {
			utils.ContextAbortWithMistake(ctx, http.StatusUnauthorized)
			return
		}

		auth := strings.Split(string(decoded), ":")

		if len(auth) != 2 {
			utils.ContextAbortWithMistake(ctx, http.StatusUnauthorized)
			return
		}

		user := models.User{
			Username: auth[0],
			Password: utils.MultiphaseHash(constants.HashSalt+auth[1], constants.HashPass),
		}

		if err := db.Where(&user).First(&user).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				utils.ContextAbortWithMistake(ctx, http.StatusUnauthorized)
				return
			}

			utils.ContextAbortWithError(ctx, err)
			return
		}

		ctx.Set(constants.CtxUser, user)
	}
}

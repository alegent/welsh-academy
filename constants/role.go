package constants

// Role
// ## Note
// I choose string here instead of integer, because it will be inserted in database.
// However, if we use an integer instead and for some reason decide to change the order,
// we will have different role assigned for user that is already inserted in the database.
type Role string

const (
	Basic        Role = "BASIC"
	CheddarLover Role = "CHEDDAR_LOVER"
	Admin        Role = "ADMIN"
)

package constants

const (
	CtxDb          = "welsh_db"
	CtxUser        = "welsh_user"
	CtxUriBind     = "welsh_uri_bind"
	CtxPageBind    = "welsh_page_bind"
	CtxRequestBind = "welsh_request_bind"
)

# Welsh Academy - OVH Technical Test

## Quick Start

You can quickly build and start the project through docker:

```shell
docker build -t welsh_academy .
docker run --name welsh_academy --rm -d -p 8080:8080 -e WELSH_USERNAME=admin -e WELSH_PASSWORD=admin welsh_academy
```

If you have go installed on your machine, you can run it with:

```shell
go get .
WELSH_USERNAME=admin WELSH_PASSWORD=admin go run .
```

Either way, you'll should pass the env variables `WELSH_USERNAME` and `WELSH_PASSWORD`. This will create a user with the
corresponding values and `ADMIN` role, so you can create more users, ingredients and recipes. This is because I used an
in memory SQLite for this project, and without at least one user, you won't be able to do much.

Finally, you should check out the Postman collection that I've provided at the root of this repo. This contains all
available requests, with some pre-script to create necessaries value and just be able to tests without copying ids
everywhere.

## Notes

### Preamble

This project was really the first big project I write in Go. I found a lot of similarities with some other languages and
didn't have some much trouble working with it, however I have probably missed some good practice or at least better ways
to do certain things.

### Timeline

* Drafting: I did make a "draft" of what the API should look like using OpenAPI. Some things have changed during
  development, so the file may not be up-to-date, but I've left it anyway. This has allowed me to know exactly what to
  test and what to do, and I certainly avoid to make some mistakes that could require me to change a lot of codes
  afterwards.
* Modeling: I write in Go all the requests, responses and models types I have defined in my OpenAPI. I did that first
  because I needed it for the next step
* Testing: this is where most of my time has gone, taking roughly 50 to 70% of the project. Every endpoint has been
  declined through this, which make the rest of the projects a lot easier and faster.
* Coding: I've actually implemented the API. Since I already have the tests at this point, this has been really quickly,
  and I could spend time to make handy functions and middlewares which make the controllers codes somewhat cleaner (or
  at least with less duplicated code).
* Refactoring: I spent some time to clean furthermore the code. However, I still don't feel satisfy by the way it is
  today. Especially in regard to the validations, that has clearly been rushed.

### MVC

I choose to structure the project around the MVC pattern because it is well known, has work well for other APIs I've
done in the past, and has work well here.

However, I want to precise that usually I structure my files by resources, likes the following:

```
.
├── ingredients
│ ├── controller.go
│ ├── ingredient.go
│ ├── requests.go
│ └── responses.go
├── recipes
│ ├── controller.go
│ ├── recipe.go
│ ├── requests.go
│ └── responses.go
└── users
    ├── controller.go
    ├── requests.go
    ├── responses.go
    └── user.go
```

The reason I didn't did it here is more or less because of Go, and the import cycle problem. If I want to have a list
of starred recipes in the user AND have the creation user in the recipe, this conflict appears quite naturally.
Resolving that implies that all entities needs to be provided by the same package, which became `models` in the current
state of the projects.

### Authentication

The authentication is made with a `Basic` header because implementing an RFC compliant OAuth2 authentication process was
obviously outside the scope. I also noted that I should not care about role implementation because "the Cheddar world
is a kind world", but I didn't quite understand what was expected of me. Since it's not that complex, I decided to
implement a role management.

There's 3 role in the API:

* `BASIC`: Can star or unstar recipes
* `CHEDDAR_LOVER`: Has full access to ingredients and recipes management, including deleting ones.
* `ADMIN`: Has full access to user management, including deleting ones.

There's also some endpoint that don't required authentication at all, like ingredients or recipes listing. My point of
view is that a traditional recipe websites won't ask for a user in order to see the recipes. It only required it when
you want to manage favorites.

### Star system

I don't have much to say, except that was heavily inspired by the GitHub API.

### Dependencies

#### SQLite

I choose SQLite mainly for the convenience of being in memory only. That implies that I didn't need to ship a Postgres
or MySql through a docker compose, I just have one container.

#### Gin

I choose Gin because it seems to be by far the most popular web framework for Go out there. By quick lookups to
documentation and videos online, it seems to respond pretty correctly to all the need I had for this project.

#### GORM

I choose GORM because it's the first one that pop up when we type `golang orm` in DuckDuckGo.

#### Testify Assert

I think that test code are as important (if not most) than the code ship in production. Everything that can make easier
to read or maintain is a big win for me. Testify, by avoiding all the `if condition { t.Fail() }`, has made the code
more clean.

### Some other things

* There's not `PUT` for ingredients, because there's not much to change in the model as is.
package views

import (
	"time"
)

type IngredientResponse struct {
	ID           string    `json:"id"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
	Name         string    `json:"name"`
	IsGlutenFree bool      `json:"is_gluten_free"`
	CreatedBy    string    `json:"created_by"`
}

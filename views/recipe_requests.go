package views

type NewRecipeRequest struct {
	Name        string   `json:"name"`
	Description *string  `json:"description"`
	Ingredients []string `json:"ingredients"`
}

type UpdateRecipeRequest struct {
	Description *string  `json:"description"`
	Ingredients []string `json:"ingredients"`
}

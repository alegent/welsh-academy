package views

import (
	"time"
)

type RecipeResponse struct {
	ID          string               `json:"id"`
	CreatedAt   time.Time            `json:"created_at"`
	UpdatedAt   time.Time            `json:"updated_at"`
	Name        string               `json:"name"`
	Description *string              `json:"description"`
	Ingredients []IngredientResponse `json:"ingredients"`
	CreatedBy   string               `json:"created_by"`
}

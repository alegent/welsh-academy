package views

import (
	"log"
	"net/http"
)

type Mistake struct {
	Error       MistakeError    `json:"error"`
	Description string          `json:"description"`
	Details     []MistakeDetail `json:"details"`
}

type MistakeDetail struct {
	Error       string  `json:"error"`
	Description string  `json:"description"`
	Field       *string `json:"field"`
}

type MistakeError string

const (
	BadRequest          MistakeError = "BAD_REQUEST"
	Unauthorized        MistakeError = "UNAUTHORIZED"
	Forbidden           MistakeError = "FORBIDDEN"
	NotFound            MistakeError = "NOT_FOUND"
	InternalServerError MistakeError = "INTERNAL_SERVER_ERROR"
)

// todo: does not match any more (`models.New`)
func New(status int, details ...MistakeDetail) (mistake *Mistake) {
	if details == nil {
		details = []MistakeDetail{}
	}

	switch status {
	case http.StatusBadRequest:
		mistake = &Mistake{
			Error:       BadRequest,
			Description: "Request data is invalid",
			Details:     details,
		}
	case http.StatusUnauthorized:
		mistake = &Mistake{
			Error:       Unauthorized,
			Description: "Resource required an authorized user",
			Details:     details,
		}
	case http.StatusForbidden:
		mistake = &Mistake{
			Error:       Forbidden,
			Description: "Resource is forbidden",
			Details:     details,
		}
	case http.StatusNotFound:
		mistake = &Mistake{
			Error:       NotFound,
			Description: "Resource does not exists",
			Details:     details,
		}
	case http.StatusInternalServerError:
		mistake = &Mistake{
			Error:       InternalServerError,
			Description: "Internal Server Error",
			Details:     details,
		}
	default:
		log.Fatalln("mistake is not defined for this status code")
	}

	return mistake
}

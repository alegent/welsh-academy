package views

import (
	"gitlab.com/alegent/welsh-academy/constants"
	"time"
)

type UserResponse struct {
	ID        string         `json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Username  string         `json:"username"`
	Role      constants.Role `json:"role"`
}

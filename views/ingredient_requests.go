package views

type NewIngredientRequest struct {
	Name         string `json:"name"`
	IsGlutenFree *bool  `json:"is_gluten_free"`
}

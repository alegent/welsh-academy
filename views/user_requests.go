package views

import (
	"gitlab.com/alegent/welsh-academy/constants"
)

type NewUserRequest struct {
	Username string          `json:"username"`
	Password string          `json:"password"`
	Role     *constants.Role `json:"role"`
}

type UpdateUserRequest struct {
	Password *string         `json:"password"`
	Role     *constants.Role `json:"role"`
}

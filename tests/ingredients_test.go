package tests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/alegent/welsh-academy/views"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/configurations"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
)

var CheddarIngredient = models.Ingredient{
	ID:           uuid.NewString(),
	CreatedAt:    time.Now(),
	UpdatedAt:    time.Now(),
	DeletedAt:    gorm.DeletedAt{},
	Name:         "Cheddar",
	IsGlutenFree: true,
	CreatedBy:    CheddarLoverUser.ID,
}

var BreadBunIngredient = models.Ingredient{
	ID:           uuid.NewString(),
	CreatedAt:    time.Now(),
	UpdatedAt:    time.Now(),
	DeletedAt:    gorm.DeletedAt{},
	Name:         "Bread Bun",
	IsGlutenFree: false,
	CreatedBy:    CheddarLoverUser.ID,
}

var SteakIngredient = models.Ingredient{
	ID:           uuid.NewString(),
	CreatedAt:    time.Now(),
	UpdatedAt:    time.Now(),
	DeletedAt:    gorm.DeletedAt{},
	Name:         "Steak",
	IsGlutenFree: true,
	CreatedBy:    CheddarLoverUser.ID,
}

var FriesIngredient = models.Ingredient{
	ID:           uuid.NewString(),
	CreatedAt:    time.Now(),
	UpdatedAt:    time.Now(),
	DeletedAt:    gorm.DeletedAt{},
	Name:         "Fries",
	IsGlutenFree: false,
	CreatedBy:    CheddarLoverUser.ID,
}

func TestGetIngredients(t *testing.T) {
	tcs := []struct {
		name     string
		queries  map[string]string
		expected []models.Ingredient
	}{
		{
			name: "should retrieve ingredients lists",
			expected: []models.Ingredient{
				BreadBunIngredient,
				CheddarIngredient,
				FriesIngredient,
				SteakIngredient,
			},
		},
		{
			name: "should retrieve a smaller page of ingredients",
			queries: map[string]string{
				"length": "1",
			},
			expected: []models.Ingredient{
				CheddarIngredient,
			},
		},
		{
			name: "should retrieve a different offset page of ingredients",
			queries: map[string]string{
				"length": "1",
				"offset": "2",
			},
			expected: []models.Ingredient{
				BreadBunIngredient,
			},
		},
		{
			name: "should return a filtered list of ingredients",
			queries: map[string]string{
				"s": "ched",
			},
			expected: []models.Ingredient{
				CheddarIngredient,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			req, err := http.NewRequest(http.MethodGet, "/ingredients", nil)
			assert.NoError(t, err)

			queries := req.URL.Query()

			for key, value := range tc.queries {
				queries.Add(key, value)
			}

			req.URL.RawQuery = queries.Encode()
			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, http.StatusOK, status)

			var responses []views.IngredientResponse
			err = json.Unmarshal(res.Body.Bytes(), &responses)
			assert.NoError(t, err)
			assert.Equal(t, len(tc.expected), len(responses))

			for _, expected := range tc.expected {
				actual := utils.First(responses, func(ingredient views.IngredientResponse) bool {
					return expected.ID == ingredient.ID
				})

				assertIngredientResponse(t, &expected, actual)
			}
		})
	}
}

func TestGetIngredient(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		id       string
		expected models.Ingredient
		mistake  views.Mistake
	}{
		{
			name:     "should retrieve an ingredient",
			status:   http.StatusOK,
			id:       BreadBunIngredient.ID,
			expected: BreadBunIngredient,
		},
		{
			name:    "should return a not found mistake",
			status:  http.StatusNotFound,
			id:      uuid.NewString(),
			mistake: *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/ingredients/%s", tc.id)
			req, err := http.NewRequest(http.MethodGet, endpoint, nil)
			assert.NoError(t, err)

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var response views.IngredientResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertIngredientResponse(t, &tc.expected, &response)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestNewIngredient(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		request  views.NewIngredientRequest
		expected models.Ingredient
		mistake  views.Mistake
	}{
		{
			name:     "should create an ingredient (cheddar lover)",
			status:   http.StatusCreated,
			username: CheddarLoverUser.Username,
			request: views.NewIngredientRequest{
				Name:         "Guacamole",
				IsGlutenFree: utils.Ref(true),
			},
			expected: models.Ingredient{
				Name:         "Guacamole",
				IsGlutenFree: true,
				CreatedBy:    CheddarLoverUser.ID,
			},
		},
		{
			name:     "should create an ingredient (admin)",
			status:   http.StatusCreated,
			username: AdminUser.Username,
			request: views.NewIngredientRequest{
				Name:         "Guacamole",
				IsGlutenFree: utils.Ref(true),
			},
			expected: models.Ingredient{
				Name:         "Guacamole",
				IsGlutenFree: true,
				CreatedBy:    AdminUser.ID,
			},
		},
		{
			name:     "should create a none gluten free ingredient (default)",
			status:   http.StatusCreated,
			username: CheddarLoverUser.Username,
			request: views.NewIngredientRequest{
				Name: "Guacamole",
			},
			expected: models.Ingredient{
				Name:         "Guacamole",
				IsGlutenFree: false,
				CreatedBy:    CheddarLoverUser.ID,
			},
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a bad request mistake (invalid name)",
			status:   http.StatusBadRequest,
			username: CheddarLoverUser.Username,
			request: views.NewIngredientRequest{
				Name: "",
			},
			mistake: *views.New(http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_NAME",
				Description: "'name' cannot be empty or invalid",
				Field:       utils.Ref("name"),
			}),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			body, err := json.Marshal(tc.request)
			assert.NoError(t, err)

			req, err := http.NewRequest(http.MethodPost, "/ingredients", bytes.NewBuffer(body))
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusCreated {
				var response views.IngredientResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertIngredientResponse(t, &tc.expected, &response)

				var ingredient models.Ingredient
				result := db.Where(models.Ingredient{ID: response.ID}).First(&ingredient)
				assert.NoError(t, result.Error)
				assertIngredient(t, &tc.expected, &ingredient)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestDeleteIngredient(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		id       string
		mistake  views.Mistake
	}{
		{
			name:     "should delete an ingredient (cheddar lover)",
			status:   http.StatusNoContent,
			username: CheddarLoverUser.Username,
			id:       CheddarIngredient.ID,
		},
		{
			name:     "should delete an ingredient (admin)",
			status:   http.StatusNoContent,
			username: AdminUser.Username,
			id:       BreadBunIngredient.ID,
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			id:      CheddarIngredient.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			id:       CheddarIngredient.ID,
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			id:       CheddarIngredient.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: CheddarLoverUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/ingredients/%s", tc.id)
			req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusNoContent {
				var ingredient models.Ingredient
				result := db.Where(models.Ingredient{ID: tc.id}).First(&ingredient)
				assert.True(t, errors.Is(result.Error, gorm.ErrRecordNotFound))

				exist := db.Unscoped().Where(models.Ingredient{ID: tc.id}).First(&ingredient)
				assert.NoError(t, exist.Error)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

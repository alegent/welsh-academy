package tests

import (
	"gorm.io/gorm"
	"log"
)

func Seed(db *gorm.DB) {
	insert := func(db *gorm.DB, entity any) {
		if err := db.Create(entity).Error; err != nil {
			log.Fatalln("error inserting entity", entity, err)
		}
	}

	insert(db, CheddarIngredient)
	insert(db, BreadBunIngredient)
	insert(db, SteakIngredient)
	insert(db, FriesIngredient)
	insert(db, CheeseBurgerRecipe)
	insert(db, SteakFriesRecipe)
	insert(db, AdminUser)
	insert(db, BasicUser)
	insert(db, CheddarLoverUser)
}

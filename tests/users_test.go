package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/alegent/welsh-academy/constants"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"gitlab.com/alegent/welsh-academy/views"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/configurations"
)

const (
	Password           = "password"
	HashPassword       = "f1d0ab3e5d677dbf3839e506c95e9a35c657e99ae500a739475e6fbc4d3367fe3864966b040089059876befe188394343a60ef8bf807470cdfa5089a86f758ac"
	UpdateHashPassword = "cd4cef8198a4eb8e5e20aaa217b4cc0fe4f96abcac0b36152270d0ddebd4270a764f725101a470324ccee838741ebb66ed3ba8952ba9e6f135d1f8acc193b823"
)

var AdminUser = models.User{
	ID:        uuid.NewString(),
	CreatedAt: time.Now(),
	UpdatedAt: time.Now(),
	DeletedAt: gorm.DeletedAt{},
	Username:  "admin",
	Password:  HashPassword,
	Role:      constants.Admin,
	StarRecipes: []models.Recipe{
		CheeseBurgerRecipe,
	},
}

var CheddarLoverUser = models.User{
	ID:        uuid.NewString(),
	CreatedAt: time.Now(),
	UpdatedAt: time.Now(),
	DeletedAt: gorm.DeletedAt{},
	Username:  "cheddar_lover",
	Password:  HashPassword,
	Role:      constants.CheddarLover,
}

var BasicUser = models.User{
	ID:        uuid.NewString(),
	CreatedAt: time.Now(),
	UpdatedAt: time.Now(),
	DeletedAt: gorm.DeletedAt{},
	Username:  "basic",
	Password:  HashPassword,
	Role:      constants.Basic,
	StarRecipes: []models.Recipe{
		CheeseBurgerRecipe,
	},
}

func TestGetUser(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		id       string
		expected models.User
		mistake  views.Mistake
	}{
		{
			name:     "should retrieve itself",
			status:   http.StatusOK,
			username: BasicUser.Username,
			id:       BasicUser.ID,
			expected: BasicUser,
		},
		{
			name:     "should retrieve any user",
			status:   http.StatusOK,
			username: AdminUser.Username,
			id:       CheddarLoverUser.ID,
			expected: CheddarLoverUser,
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			id:      BasicUser.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			id:       BasicUser.ID,
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			id:       CheddarLoverUser.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a forbidden mistake (cheddar lover user)",
			status:   http.StatusForbidden,
			username: CheddarLoverUser.Username,
			id:       BasicUser.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should have a not found mistake",
			status:   http.StatusNotFound,
			username: AdminUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/users/%s", tc.id)
			req, err := http.NewRequest(http.MethodGet, endpoint, nil)
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var response views.UserResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertUserResponse(t, &tc.expected, &response)

				var user models.User
				result := db.Where(models.User{ID: response.ID}).First(&user)
				assert.NoError(t, result.Error)
				assertUser(t, &tc.expected, &user)

				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestNewUser(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		request  views.NewUserRequest
		expected models.User
		mistake  views.Mistake
	}{
		{
			name:     "should create a basic user",
			status:   http.StatusCreated,
			username: AdminUser.Username,
			request: views.NewUserRequest{
				Username: "basic_user",
				Password: Password,
			},
			expected: models.User{
				Username: "basic_user",
				Password: HashPassword,
				Role:     constants.Basic,
			},
		},
		{
			name:     "should create a user with a non basic role",
			status:   http.StatusCreated,
			username: AdminUser.Username,
			request: views.NewUserRequest{
				Username: "non_basic_user",
				Password: Password,
				Role:     utils.Ref(constants.CheddarLover),
			},
			expected: models.User{
				Username: "non_basic_user",
				Password: HashPassword,
				Role:     constants.CheddarLover,
			},
		},
		{
			name:    "should return an unauthorized mistake (no auth)",
			status:  http.StatusUnauthorized,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a forbidden mistake (cheddar lover user)",
			status:   http.StatusForbidden,
			username: CheddarLoverUser.Username,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should have an invalid username",
			status:   http.StatusBadRequest,
			username: AdminUser.Username,
			request: views.NewUserRequest{
				Username: "",
				Password: Password,
			},
			mistake: *views.New(http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_USERNAME",
				Description: "username should not be empty or invalid",
				Field:       utils.Ref("username"),
			}),
		},
		{
			name:     "should have an invalid password",
			status:   http.StatusBadRequest,
			username: AdminUser.Username,
			request: views.NewUserRequest{
				Username: "invalid_password",
				Password: "",
			},
			mistake: *views.New(http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_PASSWORD",
				Description: "password should not be empty or invalid",
				Field:       utils.Ref("password"),
			}),
		},
		{
			name:     "should have multiple errors (username and password)",
			status:   http.StatusBadRequest,
			username: AdminUser.Username,
			mistake: *views.New(
				http.StatusBadRequest,
				views.MistakeDetail{
					Error:       "INVALID_PASSWORD",
					Description: "password should not be empty or invalid",
					Field:       utils.Ref("password"),
				}, views.MistakeDetail{
					Error:       "INVALID_USERNAME",
					Description: "username should not be empty or invalid",
					Field:       utils.Ref("username"),
				}),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			body, err := json.Marshal(tc.request)
			assert.NoError(t, err)

			req, err := http.NewRequest(http.MethodPost, "/users", bytes.NewBuffer(body))
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusCreated {
				var response views.UserResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertUserResponse(t, &tc.expected, &response)

				var user models.User
				result := db.Where(models.User{ID: response.ID}).First(&user)
				assert.NoError(t, result.Error)
				assertUser(t, &tc.expected, &user)

				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestUpdateUser(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		id       string
		request  views.UpdateUserRequest
		expected models.User
		mistake  views.Mistake
	}{
		{
			name:     "should update is own password",
			status:   http.StatusOK,
			username: BasicUser.Username,
			id:       BasicUser.ID,
			request: views.UpdateUserRequest{
				Password: utils.Ref("update"),
			},
			expected: models.User{
				Username: BasicUser.Username,
				Password: UpdateHashPassword,
				Role:     constants.Basic,
			},
		},
		{
			name:     "should update another user password and role",
			status:   http.StatusOK,
			username: AdminUser.Username,
			id:       BasicUser.ID,
			request: views.UpdateUserRequest{
				Password: utils.Ref("update"),
				Role:     utils.Ref(constants.CheddarLover),
			},
			expected: models.User{
				Username: BasicUser.Username,
				Password: UpdateHashPassword,
				Role:     constants.CheddarLover,
			},
		},
		{
			name:     "should return an unauthorized mistake (no auth)",
			status:   http.StatusUnauthorized,
			id:       BasicUser.ID,
			username: "",
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			id:       BasicUser.ID,
			username: "some_unknown",
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (non admin user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			id:       CheddarLoverUser.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a forbidden mistake (try to change it's own role)",
			status:   http.StatusForbidden,
			username: CheddarLoverUser.Username,
			id:       CheddarLoverUser.ID,
			request: views.UpdateUserRequest{
				Role: utils.Ref(constants.Admin),
			},
			mistake: *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: AdminUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			body, err := json.Marshal(tc.request)
			assert.NoError(t, err)

			endpoint := fmt.Sprintf("/users/%s", tc.id)
			req, err := http.NewRequest(http.MethodPut, endpoint, bytes.NewBuffer(body))
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var response views.UserResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertUserResponse(t, &tc.expected, &response)

				var user models.User
				db = db.Where(models.User{ID: response.ID}).First(&user)
				assert.NoError(t, db.Error)
				assertUser(t, &tc.expected, &user)

				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestDeleteUser(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		id       string
		mistake  views.Mistake
	}{
		{
			name:     "should delete an user",
			status:   http.StatusNoContent,
			username: AdminUser.Username,
			id:       BasicUser.ID,
		},
		{
			name:    "should return an unauthorized mistake (no auth)",
			status:  http.StatusUnauthorized,
			id:      BasicUser.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			id:       BasicUser.ID,
			username: "some_unknown",
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (non admin user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			id:       CheddarLoverUser.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: AdminUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/users/%s", tc.id)
			req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusNoContent {
				var user models.User
				result := db.Unscoped().Where(models.User{ID: tc.id}).First(&user)
				assert.NoError(t, result.Error)

				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestGetUserRecipes(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		id       string
		expected []models.Recipe
		mistake  views.Mistake
	}{
		{
			name:   "should retrieve cheddar lover recipes",
			status: http.StatusOK,
			id:     CheddarLoverUser.ID,
			expected: []models.Recipe{
				SteakFriesRecipe, CheeseBurgerRecipe,
			},
		},
		{
			name:    "should have a not found mistake",
			status:  http.StatusNotFound,
			id:      uuid.NewString(),
			mistake: *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/users/%s/recipes", tc.id)
			req, err := http.NewRequest(http.MethodGet, endpoint, nil)
			assert.NoError(t, err)

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var responses []views.RecipeResponse
				err = json.Unmarshal(res.Body.Bytes(), &responses)
				assert.NoError(t, err)
				assert.Equal(t, len(tc.expected), len(responses))

				for _, expected := range tc.expected {
					response := utils.First(responses, func(recipe views.RecipeResponse) bool {
						return expected.ID == recipe.ID
					})

					assertRecipeResponse(t, &expected, response)

					recipe := models.Recipe{ID: expected.ID}
					result := db.Where(&recipe).First(&recipe)
					assert.NoError(t, result.Error)
					assertRecipe(t, &expected, &recipe)
				}

				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestGetUserStarRecipes(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		id       string
		expected []models.Recipe
		mistake  views.Mistake
	}{
		{
			name:   "should retrieve cheddar lover recipes",
			status: http.StatusOK,
			id:     BasicUser.ID,
			expected: []models.Recipe{
				CheeseBurgerRecipe,
			},
		},
		{
			name:    "should have a not found mistake",
			status:  http.StatusNotFound,
			id:      uuid.NewString(),
			mistake: *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/users/%s/starred", tc.id)
			req, err := http.NewRequest(http.MethodGet, endpoint, nil)
			assert.NoError(t, err)

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var responses []views.RecipeResponse
				err = json.Unmarshal(res.Body.Bytes(), &responses)
				assert.NoError(t, err)
				assert.Equal(t, len(tc.expected), len(responses))

				for _, expected := range tc.expected {
					response := utils.First(responses, func(recipe views.RecipeResponse) bool {
						return expected.ID == recipe.ID
					})

					assertRecipeResponse(t, &expected, response)

					recipe := models.Recipe{ID: expected.ID}
					result := db.Where(&recipe).First(&recipe)
					assert.NoError(t, result.Error)
					assertRecipe(t, &expected, &recipe)
				}

				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

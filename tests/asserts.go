package tests

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/utils"
	"gitlab.com/alegent/welsh-academy/views"
	"testing"
)

func assertMistake(t *testing.T, expected, actual *views.Mistake) {
	assert.Equal(t, &expected.Error, &actual.Error)
	assert.Equal(t, &expected.Description, &actual.Description)
	assert.Equal(t, len(expected.Details), len(actual.Details))

	for _, ex := range expected.Details {
		ac := utils.First(actual.Details, func(detail views.MistakeDetail) bool {
			return detail.Error == ex.Error
		})

		if !assert.NotNil(t, ac) {
			continue
		}

		assert.Equal(t, ex.Error, ac.Error)
		assert.Equal(t, ex.Description, ac.Description)
		assert.Equal(t, ex.Field, ac.Field)
	}
}

func assertIngredient(t *testing.T, expected, actual *models.Ingredient) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.IsGlutenFree, actual.IsGlutenFree)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
}

func assertIngredientResponse(t *testing.T, expected *models.Ingredient, actual *views.IngredientResponse) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.IsGlutenFree, actual.IsGlutenFree)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
}

func assertRecipe(t *testing.T, expected, actual *models.Recipe) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
}

func assertRecipeResponse(t *testing.T, expected *models.Recipe, actual *views.RecipeResponse) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
}

func assertUser(t *testing.T, expected, actual *models.User) {
	assert.Equal(t, expected.Username, actual.Username)
	assert.Equal(t, expected.Password, actual.Password)
	assert.Equal(t, expected.Role, actual.Role)
}

func assertUserResponse(t *testing.T, expected *models.User, actual *views.UserResponse) {
	assert.Equal(t, expected.Username, actual.Username)
	assert.Equal(t, expected.Role, actual.Role)
}

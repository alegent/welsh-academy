package tests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/alegent/welsh-academy/models"
	"gitlab.com/alegent/welsh-academy/views"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/alegent/welsh-academy/configurations"
	"gitlab.com/alegent/welsh-academy/utils"
)

var CheeseBurgerRecipe = models.Recipe{
	ID:          uuid.NewString(),
	CreatedAt:   time.Now(),
	UpdatedAt:   time.Now(),
	DeletedAt:   gorm.DeletedAt{},
	Name:        "Cheese Burger",
	Description: utils.Ref("A burger with cheese"),
	Ingredients: []models.Ingredient{
		CheddarIngredient, BreadBunIngredient,
	},
	CreatedBy: CheddarLoverUser.ID,
}

var SteakFriesRecipe = models.Recipe{
	ID:          uuid.NewString(),
	CreatedAt:   time.Now(),
	UpdatedAt:   time.Now(),
	DeletedAt:   gorm.DeletedAt{},
	Name:        "Steak Fries",
	Description: utils.Ref("Steak, fries, happiness"),
	Ingredients: []models.Ingredient{
		SteakIngredient, FriesIngredient,
	},
	CreatedBy: CheddarLoverUser.ID,
}

func TestIsGlutenFree(t *testing.T) {
	tcs := []struct {
		name     string
		recipe   models.Recipe
		expected bool
	}{
		{
			name: "should be gluten free",
			recipe: models.Recipe{
				Ingredients: []models.Ingredient{
					{IsGlutenFree: true},
					{IsGlutenFree: true},
				},
			},
			expected: true,
		},
		{
			name: "should not be gluten free",
			recipe: models.Recipe{
				Ingredients: []models.Ingredient{
					{IsGlutenFree: false},
					{IsGlutenFree: false},
				},
			},
			expected: false,
		},
		{
			name: "should not be gluten free with at least one not gluten free",
			recipe: models.Recipe{
				Ingredients: []models.Ingredient{
					{IsGlutenFree: true},
					{IsGlutenFree: true},
					{IsGlutenFree: true},
					{IsGlutenFree: false},
				},
			},
			expected: false,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			assert.Equal(t, tc.expected, tc.recipe.IsGlutenFree())
		})
	}
}

func TestGetRecipes(t *testing.T) {
	tcs := []struct {
		name     string
		queries  map[string]string
		expected []models.Recipe
	}{
		{
			name: "should retrieve recipes lists",
			expected: []models.Recipe{
				CheeseBurgerRecipe, SteakFriesRecipe,
			},
		},
		{
			name: "should retrieve a smaller page of recipes",
			queries: map[string]string{
				"length": "1",
			},
			expected: []models.Recipe{
				CheeseBurgerRecipe,
			},
		},
		{
			name: "should retrieve a different offset page of recipes",
			queries: map[string]string{
				"length": "1",
				"offset": "2",
			},
			expected: []models.Recipe{
				SteakFriesRecipe,
			},
		},
		{
			name: "should return a filtered list of recipes",
			queries: map[string]string{
				"s": "chee",
			},
			expected: []models.Recipe{
				CheeseBurgerRecipe,
			},
		},
		// todo maybe also search for ingredient in the recipes
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			req, err := http.NewRequest(http.MethodGet, "/recipes", nil)
			assert.NoError(t, err)

			queries := req.URL.Query()
			for key, value := range tc.queries {
				queries.Add(key, value)
			}

			req.URL.RawQuery = queries.Encode()
			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, http.StatusOK, status)

			var responses []views.RecipeResponse
			err = json.Unmarshal(res.Body.Bytes(), &responses)
			assert.NoError(t, err)
			assert.Equal(t, len(tc.expected), len(responses))

			for _, expected := range tc.expected {
				actual := utils.First(responses, func(recipe views.RecipeResponse) bool {
					return expected.ID == recipe.ID
				})

				assertRecipeResponse(t, &expected, actual)
			}
		})
	}
}

func TestGetRecipe(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		id       string
		expected models.Recipe
		mistake  views.Mistake
	}{
		{
			name:     "should retrieve an recipe",
			status:   http.StatusOK,
			id:       SteakFriesRecipe.ID,
			expected: SteakFriesRecipe,
		},
		{
			name:    "should return a not found mistake",
			status:  http.StatusNotFound,
			id:      uuid.NewString(),
			mistake: *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/recipes/%s", tc.id)
			req, err := http.NewRequest(http.MethodGet, endpoint, nil)
			assert.NoError(t, err)

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var response views.RecipeResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertRecipeResponse(t, &tc.expected, &response)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestNewRecipe(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		request  views.NewRecipeRequest
		expected models.Recipe
		mistake  views.Mistake
	}{
		{
			name:     "should create a new recipe (cheddar lover)",
			status:   http.StatusCreated,
			username: CheddarLoverUser.Username,
			request: views.NewRecipeRequest{
				Name: "Cheddar + Fries",
				Ingredients: []string{
					FriesIngredient.ID,
					CheddarIngredient.ID,
				},
			},
			expected: models.Recipe{
				Name:      "Cheddar + Fries",
				CreatedBy: CheddarLoverUser.ID,
				Ingredients: []models.Ingredient{
					FriesIngredient,
					CheddarIngredient,
				},
			},
		},
		{
			name:     "should create a recipes (admin)",
			status:   http.StatusCreated,
			username: AdminUser.Username,
			request: views.NewRecipeRequest{
				Name: "Just BreadBun ???",
				Ingredients: []string{
					BreadBunIngredient.ID,
				},
			},
			expected: models.Recipe{
				Name:      "Just BreadBun ???",
				CreatedBy: AdminUser.ID,
				Ingredients: []models.Ingredient{
					BreadBunIngredient,
				},
			},
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a bad request mistake (invalid name)",
			status:   http.StatusBadRequest,
			username: CheddarLoverUser.Username,
			request: views.NewRecipeRequest{
				Name: "",
				Ingredients: []string{
					FriesIngredient.ID,
				},
			},
			mistake: *views.New(http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_NAME",
				Description: "'name' cannot be empty or invalid",
				Field:       utils.Ref("name"),
			}),
		},
		{
			name:     "should return a bad request mistake (empty list of ingredients)",
			status:   http.StatusBadRequest,
			username: CheddarLoverUser.Username,
			request: views.NewRecipeRequest{
				Name:        "Empty",
				Ingredients: []string{},
			},
			mistake: *views.New(http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_INGREDIENTS",
				Description: "'ingredients' cannot be empty or invalid",
				Field:       utils.Ref("ingredients"),
			}),
		},
		{
			name:     "should return a bad request mistake (ingredients does not exists)",
			status:   http.StatusBadRequest,
			username: CheddarLoverUser.Username,
			request: views.NewRecipeRequest{
				Name: "Invalid",
				Ingredients: []string{
					uuid.NewString(),
				},
			},
			mistake: *views.New(http.StatusBadRequest, views.MistakeDetail{
				Error:       "INVALID_INGREDIENTS",
				Description: "'ingredients' cannot be empty or invalid",
				Field:       utils.Ref("ingredients"),
			}),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			body, err := json.Marshal(tc.request)
			assert.NoError(t, err)

			req, err := http.NewRequest(http.MethodPost, "/recipes", bytes.NewBuffer(body))
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusCreated {
				var response views.RecipeResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertRecipeResponse(t, &tc.expected, &response)

				var recipe models.Recipe
				result := db.Where(&models.Recipe{ID: response.ID}).First(&recipe)
				assert.NoError(t, result.Error)
				assertRecipe(t, &tc.expected, &recipe)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestUpdateRecipe(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		id       string
		request  views.UpdateRecipeRequest
		expected models.Recipe
		mistake  views.Mistake
	}{
		{
			name:     "should update description",
			status:   http.StatusOK,
			username: CheddarLoverUser.Username,
			id:       SteakFriesRecipe.ID,
			request: views.UpdateRecipeRequest{
				Description: utils.Ref("update description"),
			},
			expected: models.Recipe{
				ID:          SteakFriesRecipe.ID,
				CreatedAt:   SteakFriesRecipe.CreatedAt,
				DeletedAt:   SteakFriesRecipe.DeletedAt,
				Name:        SteakFriesRecipe.Name,
				Description: utils.Ref("update description"),
				Ingredients: SteakFriesRecipe.Ingredients,
				CreatedBy:   SteakFriesRecipe.CreatedBy,
			},
		},
		{
			name:     "should update ingredients",
			status:   http.StatusOK,
			username: AdminUser.Username,
			id:       SteakFriesRecipe.ID,
			request: views.UpdateRecipeRequest{
				Ingredients: []string{
					SteakIngredient.ID,
				},
			},
			expected: models.Recipe{
				ID:          SteakFriesRecipe.ID,
				CreatedAt:   SteakFriesRecipe.CreatedAt,
				DeletedAt:   SteakFriesRecipe.DeletedAt,
				Name:        SteakFriesRecipe.Name,
				Description: SteakFriesRecipe.Description,
				Ingredients: []models.Ingredient{
					SteakIngredient,
				},
				CreatedBy: SteakFriesRecipe.CreatedBy,
			},
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			id:      SteakFriesRecipe.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			id:       SteakFriesRecipe.ID,
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			id:       SteakFriesRecipe.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: CheddarLoverUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			body, err := json.Marshal(tc.request)
			assert.NoError(t, err)

			endpoint := fmt.Sprintf("/recipes/%s", tc.id)
			req, err := http.NewRequest(http.MethodPut, endpoint, bytes.NewBuffer(body))
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusOK {
				var response views.RecipeResponse
				err = json.Unmarshal(res.Body.Bytes(), &response)
				assert.NoError(t, err)
				assertRecipeResponse(t, &tc.expected, &response)

				var recipe models.Recipe
				result := db.Where(&models.Recipe{ID: response.ID}).First(&recipe)
				assert.NoError(t, result.Error)
				assertRecipe(t, &tc.expected, &recipe)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestDeleteRecipe(t *testing.T) {
	tcs := []struct {
		name     string
		status   int
		username string
		id       string
		mistake  views.Mistake
	}{
		{
			name:     "should delete a recipe (cheddar lover)",
			status:   http.StatusNoContent,
			username: CheddarLoverUser.Username,
			id:       SteakFriesRecipe.ID,
		},
		{
			name:     "should delete a recipe (admin)",
			status:   http.StatusNoContent,
			username: AdminUser.Username,
			id:       CheeseBurgerRecipe.ID,
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			id:      SteakFriesRecipe.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			id:       SteakFriesRecipe.ID,
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a forbidden mistake (basic user)",
			status:   http.StatusForbidden,
			username: BasicUser.Username,
			id:       SteakFriesRecipe.ID,
			mistake:  *views.New(http.StatusForbidden),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: CheddarLoverUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/recipes/%s", tc.id)
			req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusNoContent {
				var recipe models.Recipe
				result := db.Where(models.Recipe{ID: tc.id}).First(&recipe)
				assert.True(t, errors.Is(result.Error, gorm.ErrRecordNotFound))

				exist := db.Unscoped().Where(models.Recipe{ID: tc.id}).First(&recipe)
				assert.NoError(t, exist.Error)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestStarRecipe(t *testing.T) {
	tcs := []struct {
		name     string
		id       string
		status   int
		username string
		mistake  views.Mistake
	}{
		{
			name:     "should star recipe (basic)",
			id:       SteakFriesRecipe.ID,
			status:   http.StatusNoContent,
			username: BasicUser.Username,
		},
		{
			name:     "should star recipe (cheddar lover)",
			id:       SteakFriesRecipe.ID,
			status:   http.StatusNoContent,
			username: CheddarLoverUser.Username,
		},
		{
			name:     "should star recipe (admin)",
			id:       SteakFriesRecipe.ID,
			status:   http.StatusNoContent,
			username: AdminUser.Username,
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			id:      SteakFriesRecipe.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			id:       SteakFriesRecipe.ID,
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: CheddarLoverUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/recipes/%s/star", tc.id)
			req, err := http.NewRequest(http.MethodPut, endpoint, nil)
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusNoContent {
				var user models.User
				db := db.Preload("StarRecipes").Where(models.User{Username: tc.username}).First(&user)
				assert.NoError(t, db.Error)

				recipe := utils.First(user.StarRecipes, func(recipe models.Recipe) bool {
					return recipe.ID == tc.id
				})

				assert.NotNil(t, recipe)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}

func TestUnstarRecipe(t *testing.T) {
	tcs := []struct {
		name     string
		id       string
		status   int
		username string
		mistake  views.Mistake
	}{
		{
			name:     "should unstar recipe (basic)",
			id:       CheeseBurgerRecipe.ID,
			status:   http.StatusNoContent,
			username: BasicUser.Username,
		},
		{
			name:     "should unstar recipe (cheddar lover)",
			id:       CheeseBurgerRecipe.ID,
			status:   http.StatusNoContent,
			username: CheddarLoverUser.Username,
		},
		{
			name:     "should unstar recipe (admin)",
			id:       CheeseBurgerRecipe.ID,
			status:   http.StatusNoContent,
			username: AdminUser.Username,
		},
		{
			name:    "should return an unauthorized mistake",
			status:  http.StatusUnauthorized,
			id:      CheeseBurgerRecipe.ID,
			mistake: *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return an unauthorized mistake (bad auth)",
			status:   http.StatusUnauthorized,
			username: "some_unknown",
			id:       CheeseBurgerRecipe.ID,
			mistake:  *views.New(http.StatusUnauthorized),
		},
		{
			name:     "should return a not found mistake",
			status:   http.StatusNotFound,
			username: CheddarLoverUser.Username,
			id:       uuid.NewString(),
			mistake:  *views.New(http.StatusNotFound),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			db := configurations.WelshDatabase()
			defer configurations.CloseDatabase(db)
			Seed(db)

			server := configurations.WelshServer(db)

			endpoint := fmt.Sprintf("/recipes/%s/star", tc.id)
			req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
			assert.NoError(t, err)

			if tc.username != "" {
				req.SetBasicAuth(tc.username, Password)
			}

			res := httptest.NewRecorder()
			server.ServeHTTP(res, req)

			status := res.Result().StatusCode
			assert.Equal(t, tc.status, status)

			if tc.status == http.StatusNoContent {
				var user models.User
				db := db.Where(models.User{Username: tc.username}).First(&user)
				assert.NoError(t, db.Error)

				recipe := utils.First(user.StarRecipes, func(recipe models.Recipe) bool {
					return recipe.ID == tc.id
				})

				assert.Nil(t, recipe)
				return
			}

			var response views.Mistake
			err = json.Unmarshal(res.Body.Bytes(), &response)
			assert.NoError(t, err)
			assertMistake(t, &tc.mistake, &response)
		})
	}
}
